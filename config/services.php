<?php

return [
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN', ''),
        'secret' => env('MAILGUN_SECRET', '')
    ],
    'api' => [
        'mojang' => [
            'base_uri' => env('MOJANG_API_BASE', '')
        ],
        'mc_api' => [
            'base_uri' => env('MC_API_BASE', '')
        ]
    ]
];
