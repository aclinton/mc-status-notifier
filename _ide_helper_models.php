<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Server
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $ip
 * @property int|null $port
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivityRecord[] $activityRecords
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server wherePort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereUpdatedAt($value)
 */
	class Server extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ActivityRecord
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $logged_in_at
 * @property \Illuminate\Support\Carbon|null $logged_out_at
 * @property int|null $player_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $server_id
 * @property-read bool $login_event
 * @property-read bool $logout_event
 * @property-read \App\Models\Player|null $player
 * @property-read \App\Models\Server|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord forPlayer($player)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord forRange(\Carbon\Carbon $start, \Carbon\Carbon $end)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord forServer($server)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereLoggedOutAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord wherePlayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereServerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActivityRecord whereUpdatedAt($value)
 */
	class ActivityRecord extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Player
 *
 * @property int $id
 * @property string|null $uuid
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivityRecord[] $activityRecords
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Player whereUuid($value)
 */
	class Player extends \Eloquent {}
}

