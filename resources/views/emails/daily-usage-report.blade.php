@component('mail::message')
#Total Time
##{{$server->name}}
##{{$timestamp->format('D, M  jS Y')}}

@component('mail::table')
| Player | Time Played |
| ------ | :-----------: |
@foreach($times as $record)
    | {{$record['player']->name}} | {{$record['total_time']}} |
@endforeach
@endcomponent

@endcomponent