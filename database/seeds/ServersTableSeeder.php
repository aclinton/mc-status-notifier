<?php

use Illuminate\Database\Seeder;

class ServersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('servers') as $key => $value) {
            \App\Models\Server::firstOrCreate([
                'name' => $key,
                'ip'   => $value['ip'],
                'port' => $value['port']
            ]);
        }
    }
}
