<?php


namespace App;

use App\Models\Server;
use Illuminate\Support\Facades\Cache;

class PlayerListManager
{
    /**
     * @param Server $server
     * @param array $player_list
     * @return array
     */
    public function newPlayers(Server $server, array $player_list): array
    {
        $old_list = Cache::get('players:' . $server->id, []);

        return array_diff($player_list, $old_list);
    }

    /**
     * @param Server $server
     * @param array $player_list
     */
    public function updatePlayers(Server $server, array $player_list): void
    {
        Cache::put('players:' . $server->id, $player_list, 10);
    }
}