<?php


namespace App\Mail;


use App\Models\Player;
use App\Models\Server;
use App\Services\PlayerActivityService;
use App\Services\ServerService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DailyUsageReport extends Mailable implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var Server
     */
    public $server;

    /**
     * @var Carbon
     */
    public $timestamp;

    /**
     * @var PlayerActivityService
     */
    private $player_activity_service;

    /**
     * @var ServerService
     */
    private $server_service;

    /**
     * DailyUsageReport constructor.
     *
     * @param Server $server
     * @param Carbon $timestamp
     */
    public function __construct(Server $server, Carbon $timestamp)
    {
        $this->server                  = $server;
        $this->timestamp               = $timestamp;
        $this->player_activity_service = new PlayerActivityService();
        $this->server_service          = new ServerService();
    }

    public function build()
    {
        $players = $this->server_service->getAllPlayers($this->server);
        $data    = $players->map(function(Player $p) {
            $seconds = $this->_getPlayersTotalTime($p);

            return [
                'player'     => $p,
                'seconds'    => $seconds,
                'total_time' => gmdate('H:i', $seconds)
            ];
        })->sortByDesc('seconds');

        return $this->markdown('emails.daily-usage-report')->with([
            'times' => $data
        ])->subject('Daily Server Report');
    }

    /**
     * @param Player $player
     * @return int
     */
    private function _getPlayersTotalTime(Player $player): int
    {
        return $this->player_activity_service->getTotalPlayingTime(
            $this->player_activity_service->getActivityForServerForDay(
                $player,
                $this->server,
                $this->timestamp
            )
        );
    }
}