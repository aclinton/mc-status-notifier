<?php


namespace App\Mail;


use Illuminate\Mail\Mailable;

class WeeklyUsageReport extends Mailable
{
    public function build()
    {
        return $this->markdown('emails.weekly-usage-report')->subject('Weekly Server Report');
    }
}