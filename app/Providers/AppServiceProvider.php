<?php

namespace App\Providers;

use App\Services\MCApi\McApi;
use App\Services\MojangApi\Mojang;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (in_array($this->app->environment(), ['local', 'dev'])) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->register(\Nexmo\Laravel\NexmoServiceProvider::class);
        $this->app->register(\Vluzrmos\Tinker\TinkerServiceProvider::class);

        $this->app->singleton(Mojang::class, function($app) {
            return new Mojang(new \App\Services\MojangApi\Client());
        });

        $this->app->singleton(McApi::class, function($app) {
            return new McApi(new \App\Services\MCApi\Client());
        });
    }
}
