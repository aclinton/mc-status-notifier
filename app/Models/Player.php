<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activityRecords(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ActivityRecord::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function loginRecords()
    {
        return $this->activityRecords()->whereNotNull('logged_in_at')->orderBy('logged_in_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function logoutRecords()
    {
        return $this->activityRecords()->whereNotNull('logged_out_at')->orderBy('logged_out_at');
    }
}