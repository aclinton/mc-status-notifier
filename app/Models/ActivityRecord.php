<?php


namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ActivityRecord extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $appends = ['login_event', 'logout_event'];

    /**
     * @var array
     */
    protected $dates = ['logged_in_at', 'logged_out_at'];

    /**
     * @return bool
     */
    public function getLoginEventAttribute(): bool
    {
        return $this->logged_in_at !== null;
    }

    /**
     * @return bool
     */
    public function getLogoutEventAttribute(): bool
    {
        return $this->logged_out_at !== null;
    }

    /**
     * @return BelongsTo
     */
    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class);
    }

    /**
     * @return BelongsTo
     */
    public function server(): BelongsTo
    {
        return $this->belongsTo(Server::class);
    }

    /**
     * @param Builder $q
     * @param mixed $server
     * @return Builder
     */
    public function scopeForServer(Builder $q, $server): Builder
    {
        return $q->whereHas('server', function (Builder $q) use($server) {
            if($server instanceof Server) {
                $q->where('id', $server->id);
            } else if(is_string($server)) {
                $q->where('name', $server);
            } else {
                $q->where('id', $server);
            }
        });
    }

    /**
     * @param Builder $q
     * @param $player
     * @return Builder
     */
    public function scopeForPlayer(Builder $q, $player): Builder
    {
        return $q->whereHas('player', function (Builder $q) use($player) {
            if($player instanceof Player) {
                $q->where('id', $player->id);
            } else if(is_string($player)) {
                $q->where('name', $player);
            } else {
                $q->where('id', $player);
            }
        });
    }

    /**
     * @param Builder $q
     * @param Carbon $start
     * @param Carbon $end
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeForRange(Builder $q, Carbon $start, Carbon $end)
    {
        return $q->whereDate('created_at', '>=', $start->toDateTimeString())
                   ->whereDate('created_at', '<=', $end->toDateTimeString());
    }
}