<?php


namespace App\Services;


use App\Models\ActivityRecord;
use App\Models\Player;
use App\Models\Server;
use Illuminate\Support\Collection;

class ServerService
{
    /**
     * @param Server $server
     * @return Collection
     */
    public function getAllPlayers(Server $server): Collection
    {
        return Player::whereIn('id', function(\Illuminate\Database\Query\Builder $q) use($server) {
            $q->from((new ActivityRecord())->getTable())
              ->where('server_id', $server->id)
              ->groupBy('player_id')
              ->select('player_id')
              ->get();
        })->get();
    }
}