<?php


namespace App\Services\MCApi\Traits;


use App\Services\MCApi\Parameters\QueryParameters;
use App\Services\MCApi\Requests\QueryRequest;

trait QueryResource
{
    /**
     * @return \App\Services\MCApi\Requests\QueryRequest
     */
    public function query(): QueryRequest
    {
        return new QueryRequest($this->client);
    }

    /**
     * @return \App\Services\MCApi\Parameters\QueryParameters
     */
    public function queryParams(): QueryParameters
    {
        return new QueryParameters();
    }
}