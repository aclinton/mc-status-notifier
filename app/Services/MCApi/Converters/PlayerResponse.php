<?php


namespace App\Services\MCApi\Converters;


use App\Services\MCApi\Dto\Players;

class PlayerResponse
{
    /**
     * @param \stdClass $json
     * @return Players
     */
    public function process(\stdClass $json): Players
    {
        $data       = new Players();
        $data->list = $json->list ?? [];
        $data->max  = $json->max ?? null;
        $data->now  = $json->now ?? null;

        return $data;
    }
}