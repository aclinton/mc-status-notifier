<?php


namespace App\Services\MCApi\Converters;


use App\Services\MCApi\Dto\Query;

class QueryConverter
{
    /**
     * @param \stdClass $json
     * @return Query
     */
    public function parse(\stdClass $json)
    {
        $query               = new Query();
        $query->status       = $json->status ?? null;
        $query->duration     = $json->duration ?? null;
        $query->error        = $json->error ?? null;
        $query->game_id      = $json->game_id ?? null;
        $query->game_type    = $json->game_type ?? null;
        $query->last_online  = $json->last_online ?? null;
        $query->last_updated = $json->last_updated ?? null;
        $query->duration     = $json->duration ?? null;
        $query->online       = $json->online ?? null;
        $query->motd         = $json->motd ?? null;
        $query->version      = $json->version ?? null;
        $query->server_mod   = $json->server_mod ?? null;
        $query->map          = $json->map ?? null;
        $query->players      = (new PlayerResponse())->process($json->players ?? new \stdClass());

        return $query;
    }
}