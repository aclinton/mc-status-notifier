<?php


namespace App\Services\MCApi\Requests;


use App\Services\MCApi\Parameters\QueryParameters;
use App\Services\MCApi\Responses\QueryResponse;

class QueryRequest extends BaseRequest
{
    /**
     * @param QueryParameters|null $params
     * @return QueryResponse
     */
    public function all(?QueryParameters $params = null): QueryResponse
    {
        return new QueryResponse($this->client->get('query', $params ? $params->getParameters() : []));
    }
}