<?php


namespace App\Services\MCApi\Requests;


use App\Services\MCApi\Client;

class BaseRequest
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseRequest constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}