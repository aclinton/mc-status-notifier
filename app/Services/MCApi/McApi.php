<?php


namespace App\Services\MCApi;


use App\Services\MCApi\Traits\QueryResource;

class McApi
{
    use QueryResource;

    /**
     * @var Client
     */
    protected $client;

    /**
     * McApi constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}