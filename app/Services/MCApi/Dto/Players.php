<?php


namespace App\Services\MCApi\Dto;


use App\TransferObject;

/**
 * Class Players
 *
 * @package App\Services\MCApi\Dto
 * @property int $max
 * @property int $now
 * @property array $list
 */
class Players extends TransferObject
{
    /**
     * Integers
     * @var
     */
    protected $max, $now;

    /**
     * Arrays
     * @var
     */
    protected $list;
}