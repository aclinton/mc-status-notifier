<?php


namespace App\Services\MCApi\Dto;


use App\TransferObject;

/**
 * Class Query
 *
 * @package App\Services\MCApi\Dto
 * @property string $status
 * @property string $error
 * @property string $motd
 * @property string $version
 * @property string $game_type
 * @property string $game_id
 * @property string $server_mod
 * @property string $map
 * @property string $last_online
 * @property string $last_updated
 * @property string $duration
 * @property boolean $online
 * @property Players $players
 */
class Query extends TransferObject
{
    /**
     * Strings
     * @var
     */
    protected $status, $error, $motd, $version, $game_type, $game_id, $server_mod,
        $map, $last_online, $last_updated, $duration;

    /**
     * Booleans
     * @var
     */
    protected $online;

    /**
     * Objects
     * @var
     */
    protected $players;
}