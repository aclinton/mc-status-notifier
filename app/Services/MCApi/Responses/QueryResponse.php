<?php


namespace App\Services\MCApi\Responses;


use App\Services\MCApi\Converters\QueryConverter;
use App\Services\MCApi\Dto\Query;

class QueryResponse extends Response
{
    /**
     * @return Query;
     */
    public function query(): Query
    {
        return $this->data;
    }

    protected function convertBody(): void
    {
        $this->data = (new QueryConverter())->parse($this->encodedBody());
    }
}