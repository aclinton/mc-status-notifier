<?php


namespace App\Services\MCApi\Parameters;


class QueryParameters extends ParameterBuilder
{
    /**
     * @param string $ip
     * @return QueryParameters
     */
    public function ip(string $ip = ''): QueryParameters
    {
        $this->setParameter('ip', $ip);

        return $this;
    }

    /**
     * @param string $port
     * @return QueryParameters
     */
    public function port($port = ''): QueryParameters
    {
        $this->setParameter('port', $port);

        return $this;
    }
}