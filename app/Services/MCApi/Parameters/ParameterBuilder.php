<?php


namespace App\Services\MCApi\Parameters;


abstract class ParameterBuilder
{
    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * Set Parameter
     *
     * @param $key
     * @param $value
     */
    public function setParameter($key, $value): void
    {
        $this->parameters[ $key ] = $value;
    }

    /**
     * Get Parameter
     *
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

}