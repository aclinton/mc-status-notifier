<?php


namespace App\Services\MCApi;


use App\Services\MCApi\Responses\QueryResponse;
use GuzzleHttp\Client as GuzzleClient;

class Client
{
    /**
     * @var Client
     */
    private $client;

    /**
     * MCApi constructor.
     */
    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri'   => config('services.api.mc_api.base_uri'),
            'exceptions' => false,
        ]);
    }

    /**
     * @param string $uri
     * @param array $params
     * @return mixed
     */
    public function get(string $uri, array $params = [])
    {
        return $this->client->get($uri, [
            'query' => $params
        ]);
    }

    /**
     * @param array $server
     * @return QueryResponse
     */
    public function query(array $server): QueryResponse
    {
        return new QueryResponse($this->client->get('query', [
            'query' => [
                'ip'   => $server['ip'],
                'port' => $server['port'],
            ]
        ]));
    }
}