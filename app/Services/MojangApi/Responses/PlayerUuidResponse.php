<?php


namespace App\Services\MojangApi\Responses;


use App\Services\MojangApi\Converters\PlayerUuidConverter;
use App\Services\MojangApi\Dto\PlayerUuid;

class PlayerUuidResponse extends Response
{
    /**
     * @return PlayerUuid
     */
    public function player(): PlayerUuid
    {
        return $this->data;
    }

    /**
     * Parse the body into a resource object
     */
    protected function convertBody(): void
    {
        $this->data = (new PlayerUuidConverter())->parse($this->encodedBody());
    }
}