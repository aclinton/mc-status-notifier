<?php


namespace App\Services\MojangApi\Responses;


use Psr\Http\Message\ResponseInterface;

abstract class Response
{
    private $body;
    protected $data;
    private $status;

    /**
     * Response constructor.
     *
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->body = $response->getBody()->getContents();
        $this->status = $response->getStatusCode();

        if($this->wasSuccessful()) {
            $this->convertBody();
        }
    }

    /**
     * Parse the body into a resource object
     */
    abstract protected function convertBody(): void;

    /**
     * @return bool
     */
    public function wasSuccessful(): bool
    {
        return $this->status > 199 && $this->status < 300;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function rawBody(): string
    {
        return $this->body;
    }

    /**
     * @param bool $array
     * @return mixed
     */
    public function encodedBody(bool $array = false)
    {
        return json_decode($this->body, $array);
    }
}