<?php


namespace App\Services\MojangApi;


use App\Services\MojangApi\Traits\MinecraftProfileResource;

class Mojang
{
    use MinecraftProfileResource;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Mojang constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}