<?php


namespace App\Services\MojangApi\Requests;


use App\Services\MojangApi\Client;

abstract class BaseRequest
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseRequest constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}