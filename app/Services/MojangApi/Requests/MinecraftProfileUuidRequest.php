<?php


namespace App\Services\MojangApi\Requests;

use App\Services\MojangApi\Responses\PlayerUuidResponse;

class MinecraftProfileUuidRequest extends BaseRequest
{
    /**
     * @param string $name
     * @return PlayerUuidResponse
     */
    public function find(string $name): PlayerUuidResponse
    {
        return new PlayerUuidResponse($this->client->get("users/profiles/minecraft/$name"));
    }
}