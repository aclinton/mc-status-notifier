<?php


namespace App\Services\MojangApi\Dto;


use App\TransferObject;

/**
 * Class PlayerUuid
 *
 * @package App\Services\MojangApi\Dto
 * @property string $id
 * @property string $name
 */
class PlayerUuid extends TransferObject
{
    /**
     * Strings
     * @var
     */
    protected $id, $name;
}