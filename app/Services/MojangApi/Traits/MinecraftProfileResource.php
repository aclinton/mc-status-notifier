<?php


namespace App\Services\MojangApi\Traits;


use App\Services\MojangApi\Requests\MinecraftProfileUuidRequest;

trait MinecraftProfileResource
{
    /**
     * @return \App\Services\MojangApi\Requests\MinecraftProfileUuidRequest
     */
    public function uuid(): MinecraftProfileUuidRequest
    {
        return new MinecraftProfileUuidRequest($this->client);
    }
}