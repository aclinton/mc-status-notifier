<?php


namespace App\Services\MojangApi\Converters;


use App\Services\MojangApi\Dto\PlayerUuid;
use App\TransferObject;

class PlayerUuidConverter
{
    /**
     * @param \stdClass $json
     * @return TransferObject|PlayerUuid
     */
    public function parse(\stdClass $json): TransferObject
    {
        $player       = new PlayerUuid();
        $player->id   = $json->id ?? null;
        $player->name = $json->name ?? null;

        return $player;
    }
}