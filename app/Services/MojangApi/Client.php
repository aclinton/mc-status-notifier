<?php


namespace App\Services\MojangApi;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    /**
     * @var Client
     */
    private $client;

    /**
     * MCApi constructor.
     */
    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri'   => config('services.api.mojang.base_uri'),
            'exceptions' => false,
        ]);
    }

    /**
     * @param string $uri
     * @param array $params
     * @return mixed
     */
    public function get(string $uri, array $params = [])
    {
        return $this->client->get($uri, [
            'query' => $params
        ]);
    }
}