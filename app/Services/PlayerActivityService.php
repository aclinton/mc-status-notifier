<?php


namespace App\Services;


use App\Models\ActivityRecord;
use App\Models\Player;
use App\Models\Server;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as SupportCollection;

class PlayerActivityService
{
    /**
     * @param Player $player
     * @param Server $server
     * @param Carbon $day
     * @return ActivityRecord[]|Builder[]|Collection|SupportCollection
     */
    public function getActivityForServerForWeek(Player $player, Server $server, Carbon $day)
    {
        $start_of_week = $day->startOfWeek();
        $end_of_week = clone $day;
        $end_of_week->endOfWeek();

        return $this->_getActivityForServerForRange($player, $server, $start_of_week, $end_of_week);
    }

    /**
     * @param Server $server
     * @param Player $player
     * @param Carbon $day
     * @return ActivityRecord[]|Builder[]|Collection|SupportCollection
     */
    public function getActivityForServerForDay(Player $player, Server $server, Carbon $day)
    {
        $start = clone $day;
        $start->startOfDay();
        $end = clone $day;
        $end->endOfDay();

        return $this->_getActivityForServerForRange($player, $server, $start, $end);
    }

    /**
     * @param $activities
     * @return int
     */
    public function getTotalPlayingTime(SupportCollection $activities): int
    {
        $total = 0;
        /** @var ActivityRecord $first */
        $first = $activities[0] ?? null;
        /** @var ActivityRecord $last */
        $last    = collect($activities)->last();
        $pointer = null;

        # Handle left ever from previous day
        if (optional($first)->logout_event) {
            $t = clone $first->logged_out_at;
            $t->startOfDay();
            $total += $first->logged_out_at->diffInSeconds($t);
            $activities->splice(0, 1);
        }

        # Handle playing past midnight
        if (optional($last)->login_event) {
            $t = clone $last->logged_in_at;
            $t->endOfDay();
            $total += $t->diffInSeconds($last->logged_in_at);

            $activities->pop();
        }

        $count = count($activities);
        $index = 0;
        while ($index < $count) {
            /** @var ActivityRecord $current */
            $current = $activities[$index];

            if ($pointer === null && $current->login_event) {
                $pointer = $current;
                $index++;
            } else if ($pointer !== null && $current->logout_event && $pointer->login_event) {
                $total            += $current->logged_out_at->diffInSeconds($pointer->logged_in_at);
                $next_login_index = $this->_getNextActivityLogin($activities, $index);
                $pointer          = null;

                if ($next_login_index === null || $next_login_index == 0) {
                    $index = $count;
                } else {
                    $index = $next_login_index;
                }
            } else {
                $index++;
            }
        }

        return $total;
    }

    /**
     * @param Player $player
     * @param Server $server
     * @param Carbon $start
     * @param Carbon $end
     * @return ActivityRecord[]|Builder[]|Collection|SupportCollection
     */
    private function _getActivityForServerForRange(Player $player, Server $server, Carbon $start, Carbon $end)
    {
        return ActivityRecord::forServer($server)
                             ->forPlayer($player)
                             ->forRange($start, $end)
                             ->orderBy('created_at', 'asc')
                             ->get();
    }

    /**
     * @param $activities
     * @param $start
     * @return int
     */
    private function _getNextActivityLogin($activities, $start): int
    {
        $index = 0;

        if (!$activities instanceof SupportCollection) {
            $activities = collect($activities);
        }

        $activities->each(function($a, $i) use ($start, &$index) {
            if ($i <= $start) {
                return true;
            }

            if ($a->login_event) {
                $index = $i;
                return false;
            }

            return true;
        });

        return $index;
    }
}