<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;

class SendWeeklyPlayerServerStats extends Command
{
    /**
     * @var string
     */
    protected $description = 'Send summary of server player stats';

    /**
     * @var string
     */
    protected $signature = 'send:weekly-player-usage
    {--server= : Specify the server}';

    /**
     * @return int
     */
    public function handle(): int
    {
        return 1;
    }
}