<?php


namespace App\Console\Commands;


use App\Models\Server;
use App\PlayerListManager;
use App\Services\MCApi\McApi;
use Illuminate\Console\Command;

class NotifyPlayerList extends Command
{
    /**
     * @var string
     */
    protected $description = 'This command handles sending notifications of new players on the server.';

    /**
     * @var string
     */
    protected $signature = 'notify:player-list
        {--server= : Specify the server to get stats for}';

    /**
     * @throws \Nexmo\Client\Exception\Exception
     * @throws \Nexmo\Client\Exception\Request
     * @throws \Nexmo\Client\Exception\Server
     * return mixed
     */
    public function handle()
    {
        $server  = $this->option('server');
        $servers = config('servers');

        while (empty($server) && !array_key_exists($server, $servers)) {
            $server = $this->choice('Please select a server', array_keys($servers), 0);
        }

        $local_server = Server::whereName($server)->first();

        if ($local_server === null) {
            $this->error("Could not find a server named: $server");
            return 0;
        }

        /** @var McApi $client */
        $client = app()->make(McApi::class);
        $params = $client->queryParams();
        $params->ip(config("servers.{$local_server->name}.ip"))->port(config("servers.{$local_server->name}.port"));
        $res = $client->query()->all($params);

        if ($res->wasSuccessful()) {
            $q           = $res->query();
            $manager     = new PlayerListManager();
            $players     = $q->players->list;
            $new_players = $manager->newPlayers($local_server, $players);

            if (count($new_players)) {
                $this->_handleNotify($local_server, $new_players);
            }

            $manager->updatePlayers($local_server, $players);
        }

        return 1;
    }

    /**
     * @param Server $server
     * @param array $new_players
     * @throws \Nexmo\Client\Exception\Exception
     * @throws \Nexmo\Client\Exception\Request
     * @throws \Nexmo\Client\Exception\Server
     */
    private function _handleNotify(Server $server, array $new_players): void
    {
        /** @var \Nexmo\Client $nexmo */
        $nexmo = app(\Nexmo\Client::class);
        $nexmo->message()->send([
            'to'   => '16147799600',
            'from' => config('nexmo.number'),
            'text' => "New server ({$server->name}) players: \n\n" . implode("\n", $new_players)
        ]);
    }
}