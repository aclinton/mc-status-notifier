<?php


namespace App\Console\Commands;


use App\Models\ActivityRecord;
use App\Models\Player;
use App\Models\Server;
use App\Services\MCApi\Dto\Query;
use App\Services\MCApi\McApi;
use App\Services\MojangApi\Mojang;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class FetchServerStatus extends Command
{
    /**
     * @var string
     */
    protected $description = 'This command builds player activity records for a specific server';

    /**
     * @var string
     */
    protected $signature = 'server:status
    {--server= : Specific server}';

    /**
     * @var Mojang
     */
    private $mojang_client;

    /**
     * @var McApi
     */
    private $mc_client;

    public function __construct()
    {
        parent::__construct();

        /** @var Mojang mojang_client */
        $this->mojang_client = app()->make(Mojang::class);

        /** @var McApi mc_client */
        $this->mc_client = app()->make(McApi::class);
    }

    /**
     * Run the command
     */
    public function handle(): int
    {
        $server  = $this->option('server');
        $servers = array_keys(config('servers'));

        while (empty($server) && !in_array($server, $servers)) {
            $server = $this->choice('Please select a server', $servers, 0);
        }

        $local_server = Server::whereName($server)->first();

        if ($local_server == null) {
            $this->error("Could not find $server locally");
            return 0;
        }

        $params = $this->mc_client->queryParams();
        $params->ip(config("servers.{$local_server->name}.ip"));
        $params->port(config("servers.{$local_server->name}.port"));

        $res = $this->mc_client->query()->all($params);

        if ($res->wasSuccessful()) {
            $this->_handleServer($res->query(), $local_server);
        }

        return 1;
    }

    /**
     * @param Query $query
     * @param Server $server
     */
    private function _handleServer(Query $query, Server $server): void
    {
        $player_list = $query->players->list;
        $this->info(count($player_list) . ' players on the server');
        $found_players = [];

        # Loop through players currently on the server
        foreach ($player_list as $player) {
            /** @var Player|null $local_player */
            $local_player = $this->_handleLocalPlayer($player);

            if ($local_player == null) {
                Log::error('Could not store: ' . $player);
                continue;
            }

            $this->_handleLoggedInPlayer($local_player, $server);

            $found_players[] = $local_player;
        }

        $players_missing = $this->_getPlayersNotLoggedIn($found_players, $server);

        if (count($players_missing) > 0) {
            $this->info(count($players_missing) . ' players logged off');
        }

        /** @var Player $missing_player */
        foreach ($players_missing as $missing_player) {
            $this->_handleLoggedOutPlayer($missing_player, $server);
        }
    }

    /**
     * @param Player $player
     * @param Server $server
     */
    private function _handleLoggedOutPlayer(Player $player, Server $server): void
    {
        $latest = $player->activityRecords()
                         ->forServer($server)
                         ->latest()
                         ->first();

        # If player has activity and last was a login event, they must be logged out
        if ($latest !== null && $latest->login_event) {
            $activity                = new ActivityRecord();
            $activity->logged_out_at = Carbon::now();
            $activity->player()->associate($player);
            $activity->server()->associate($server);
            $activity->save();
        }
    }

    /**
     * @param Player $player
     * @param Server $server
     */
    private function _handleLoggedInPlayer(Player $player, Server $server): void
    {
        /** @var Builder|ActivityRecord $records */
        $records         = $player->activityRecords()->latest();
        $latest_activity = $records->forServer($server)->first();

        # If first seen or last activity was logout, create login event
        if ($latest_activity == null || $latest_activity->logout_event) {
            $activity               = new ActivityRecord();
            $activity->logged_in_at = Carbon::now();
            $activity->player()->associate($player);
            $activity->server()->associate($server);
            $activity->save();
        }
    }

    /**
     * @param string $name
     * @return Player
     */
    private function _handleLocalPlayer(string $name = ''): ?Player
    {
        $r = $this->mojang_client->uuid()->find($name);

        if ($r->wasSuccessful()) {
            $mc_player    = $r->player();
            $local_player = Player::whereUuid($mc_player->id)->first();

            if ($local_player == null) {
                $local_player       = new Player();
                $local_player->uuid = $mc_player->id;
                $local_player->name = $mc_player->name;
                $local_player->save();
            } else {
                $local_player->update([
                    'name' => $mc_player->name
                ]);
            }
        }

        return $local_player ?? null;
    }

    /**
     * @param array $logged_in_players
     * @param Server $server
     * @return mixed
     */
    private function _getPlayersNotLoggedIn(array $logged_in_players, Server $server)
    {
        return Player::whereIn('id', function(\Illuminate\Database\Query\Builder $q) use ($logged_in_players, $server) {
            $q->from((new ActivityRecord())->getTable())
              ->where('server_id', $server->id)
              ->whereNotIn('player_id', collect($logged_in_players)->pluck('id')->all())
              ->groupBy('player_id')
              ->select('player_id');
        })->get();
    }
}