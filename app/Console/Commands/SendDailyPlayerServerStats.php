<?php


namespace App\Console\Commands;


use App\Mail\DailyUsageReport;
use App\Models\Server;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendDailyPlayerServerStats extends Command
{
    /**
     * @var string
     */
    protected $signature = 'send:daily-player-usage
    {--server= : Specify the server by name}
    {--timestamp= : Y-m-d format}
    {--to= : Comma separated list}
    {--previous-day=yes : Yes,no for fetching previous day\'s stats}';

    /**
     * return mixed
     */
    public function handle()
    {
        $server       = $this->option('server');
        $timestamp    = $this->option('timestamp');
        $to           = $this->option('to');
        $previous_day = strtolower($this->option('previous-day'));
        $now          = Carbon::now();

        if (empty($timestamp)) {
            $timestamp = $now;
        } else {
            $timestamp = Carbon::createFromFormat('Y-m-d', $timestamp);
        }

        if (in_array($previous_day, ['yes', 'no'])) {
            if ($previous_day === 'yes') {
                $previous_day = true;
            } else {
                $previous_day = false;
            }
        } else {
            $previous_day = null;
        }

        if ($previous_day) {
            $timestamp = $now->subDay(1);
        }

        while (empty($server) && !array_key_exists($server, config('servers'))) {
            $server = $this->choice('Please select a server', array_keys(config('servers')), 0);
        }

        while (empty($to)) {
            $to = $this->ask('Please enter in a list of recipients');
        }

        $timestamp->setTimezone('America/New_York');
        $server = Server::where('name', $server)->firstOrFail();
        $this->info("Fetching {$server->name} stats for " . $timestamp->toDateTimeString());
        $mail = new DailyUsageReport($server, $timestamp);
        $to   = array_map('trim', explode(',', $to));

        Mail::to($to)->queue($mail->onQueue('emails'));

        return 1;
    }
}