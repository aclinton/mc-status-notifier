<?php

namespace App\Console;

use App\Console\Commands\FetchServerStatus;
use App\Console\Commands\NotifyPlayerList;
use App\Console\Commands\SendDailyPlayerServerStats;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        NotifyPlayerList::class,
        FetchServerStatus::class,
        SendDailyPlayerServerStats::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('notify:player-list --server=the_fam')->everyFiveMinutes();
        $schedule->command('notify:player-list --server=the_fam_v2')->everyFiveMinutes();
        $schedule->command('server:status --server=the_fam')->everyMinute();
        $schedule->command('server:status --server=the_fam_v2')->everyMinute();
        $schedule->command('send:daily-player-usage --server=the_fam --to=ddrew.clinton@gmail.com --previous-day=yes')
                 ->dailyAt('08:00')
                 ->timezone('America/New_York');
        $schedule->command('send:daily-player-usage --server=the_fam_v2 --to=ddrew.clinton@gmail.com --previous-day=yes')
                 ->dailyAt('08:00')
                 ->timezone('America/New_York');
    }
}
